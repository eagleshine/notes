---
layout: post
title: "Create a Personal Blog Site"
comments: true
date: 2016-08-17 01:10:10
categories: misc
tags:
  - blog
  - gitlab
  - jekyll
---
## Table of Contents
* [Platform Selection](#platform-selection)
* [Blog Site Building](#blog-site-building)
* [Enhancement](#enhancement)


One day I was decided to build a personal blog site to keep record
of all my learning notes. I have some initial requirements in my
mind of how this should work:

* **Easy to write**: it should be content focused. I was using a lot of
markdown in my work for posting comments. So it should support markdown
syntax for writing notes
* **Easy to host**: it should be easy to set up the website
* **Easy to view**: it should be easy to access both locally and on-line

## Platform Selection

The whole idea is to `keep it simple`. I have somehow had some impression
of [static generated website][static-gen]. There are lots of static site
generators out there. I went over some of the generator websites
and searched on google what the resulting blog site will look like.
After spending a lot of time looking at other people's blogs, I still
have a difficulty on deciding which one to use:

* [Jekyll][jekyll-site]
* [Pelican][pelican-site]
* [Hexo][hexo-site]
* [GitBook][gitbook-site]

After some struggling, I decided to follow the wisdom of the crowd, use
`Jekyll`. The other reason I choose `Jekyll` is because of the hosting
platform.

I decided to host the blog site in a git repository. I want to check
all the notes into a git repository so that I could easily obtain
all of them from different computers. In case my computer crashes, I will
not lose them. There are three most popular `git repository` hosting platform:

* [GitHub][github-site]
* [GitLab][gitlab-site]
* [BitBucket][bitbucket-site]

The most popular one is `GitHub`. But it does not provide free[^1] private repository,
while `GitLab` and `BitBucket` are providing this feature. While `GitHub` is the
most popular choice, there are some limitations. Specifically for `Jekyll`, it 
could not run `Jekyll plugins`. In the meantime, `GitLab` seems to be very 
similar to `GitHub` while it could run custom build steps,
see [example Jekyll site using GitLab Pages][gitlab-jekyll].

## Blog Site Building

I forked the respository of "[Example Jekyll website using GitLab Pages][gitlab-jekyll]".
This gives me a rapid start. I followed the instructions in the respository and cloned it
to my local machine. And after I run the command:

```bash
$jekyll serve --drafts
```

I get a blog site served locally at `http://127.0.0.1:4000/notes/` for testing. I opened
a browser and enter the url `http://eagleshine.gitlab.io/notes` and hopefully it will show
my site. However, something is wrong. It turns out that I need to add a shared runner in order 
to build and deploy the blog site.

## Enhancement

### Disqus Comments Integration
I searched the steps for integrating `disqus` with `Jekyll` website. The steps are simple and
outlined below, see [Adding Disqus to Jekyll](https://desiredpersona.com/disqus-comments-jekyll/):

* **Register in disqus**: Sign up in `disqus` and register (install) it to your website in `disqus`.
The world `install` is kind of confusing. This means to install it to the blog not your local machine.
Get a `shortname` for use in the `Jekyll` blog.

* **Adding disqus shortname**: Open `_config.yml` and adding the following code. Remember to change
`shortname` to your own Disqus shortname.

```
disqus_shortname: shortname
```

* **Adding a disqus includes**: Adding a `disqus_comments.html` in `_includes` folder with contents:

```js
{% raw %}
{% if page.comments %}
<div id="disqus_thread"></div>
<script>

/**
 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
 */
var disqus_config = function () {
    this.page.url = "{{ page.url | remove: 'index.html' | prepend: site.baseurl | prepend: site.url }}";
    this.page.identifier = '{{ page.id }}';
};

(function() {
    var d = document, s = d.createElement('script');
    s.src = '//{{ site.disqus_shortname }}.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
{% endif %}
{% endraw %}
```
* **Adding comments to post**: Adding the following code after `</article>`
in the file `post.html` in `_layouts` folder:

```
{% raw %}
{% include disqus_comments.html %}
{% endraw %}
```

* **Enable comments in a post**: add a line `comments: true` in the front matter of the post.

* **Adding comments count**: add the following code to `default.html` layout right before the closing
html tag `</body>`.

```js
<script id="dsq-count-scr" src="//{{ site.disqus_shortname }}.disqus.com/count.js" async></script>
```

Then in the `post.html` layout, add the following code to show the number of comments.

```
{% raw %}
{% if page.comments %} • <a href="{{ page.url | prepend: site.baseurl }}#disqus_thread">0 Comments</a>{% endif %}
{% endraw %}
```

### FontAwesome Integration

First add a link of font-awesome style sheet to `head.html` in `_includes`
right before `</head>` tag:

```html
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
```

Then use the font icon in the `css` or `scss` file. For example, to add a tag icon, include a 
style under tag related class:

```scss
    a:before {
      display: inline-block;
      line-height: 1.2;
      margin: 5px;
      font-family: "FontAwesome";
      content: "\f02b";
    }
```

Here `\f02b` is the unicode for <i class="fa fa-tag" aria-hidden="true"></i> icon.

Another way to add the icon is to add a `<i/>` element in source file. This needs
to change the source file.

```html
<i class="fa fa-tag" aria-hidden="true"></i>
```

[static-gen]: https://www.staticgen.com/
[jekyll-site]: http://jekyllrb.com/
[pelican-site]: http://blog.getpelican.com/
[hexo-site]: http://hexo.io/
[gitbook-site]: https://www.gitbook.com/
[github-site]: https://github.com/
[gitlab-site]: https://gitlab.com/
[bitbucket-site]: https://bitbucket.org/
[gitlab-jekyll]: https://gitlab.com/pages/jekyll


---

[^1]: Free status could change


